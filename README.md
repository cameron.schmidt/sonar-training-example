# sonar-e2e-app

This is an example Javascript application suitable for demonstrating the analysis features of SonarLint and SonarQube.

## Making changes/showing New Code scans
* The file `myserver.js` in the root directory contains a commented-out code block. Uncomment, commit, and on a fresh scan you'll have New Code results including a vulnerability.
